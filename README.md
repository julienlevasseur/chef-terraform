# terraform

Install/uninstall Terraform

## Attributes

* `version`: The Terraform version to isntall

## Usage

### Installation

By calling the default recipe:
```ruby
node.override['terraform']['version'] = "0.11.11"
include_recipe 'terraform'
```

By calling the `terraform` resource:
```ruby
node.override['terraform']['version'] = "0.11.11"
terraform node['terraform']['version']
```

### Uninstallation

By including the `uninstall` recipe in your wrapper cookbook:
```ruby
include_recipe 'terraform::uninstall'
```

### Plugins

#### Install a custom plugin

```ruby
node.override['terraform']['plugins']: [
    {
        name: "terraform-provider-gd",
        version: "0.0.1",
        source: "https://github.com/barneyparker/terraform-provider-gd/releases/download/v0.0.1/terraform-provider-gd.linux.amd64.tgz",
        destination: "/home/julien/.terraform.d/plugins"
    }
]
```