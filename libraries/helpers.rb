class Chef
  class Recipe
    module Terraform
      module Plugin
        def self.installed?(name, plugin_dir)
          return File.exist?("#{plugin_dir}/#{Architecture.get()}/#{name}")
        end
      end

      module Architecture
        def self.get()
          x = RUBY_PLATFORM.split('-')[0 ... 2]

          arch = {
            "x86_64" => "amd64",
            "i686" => "386"
          }

          return "#{x[1]}_#{arch[x[0]]}"
        end
      end
    end
  end
end
