name 'terraform'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures terraform'
long_description 'Installs/Configures terraform'
version '1.0.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

depends 'tar'
depends 'zipfile'

issues_url 'https://github.com/julienlevasseur/chef-terraform/issues'
source_url 'https://github.com/julienlevasseur/chef-terraform'

supports 'ubuntu'
supports 'centos'
supports 'debian'
