use_inline_resources

def whyrun_supported
  true
end

action :install  do
  unless terraform_is_installed
    install_terraform
  else
    if terraform_installed_version < new_resource.version
      converge_by "Updating Terraform #{terraform_installed_version} -> #{new_resource.version}" do
        begin
          cleanup
          install_terraform
          Chef::Log.debug("Updating Terraform version: from #{terraform_installed_version} to #{new_resource.version}")
        rescue
          Chef::Log.error("Failed to update Terraform from #{terraform_installed_version} to #{new_resource.version}")
        end
      end
    end
  end
end

def terraform_is_installed
  return ::File.file?("#{new_resource.install_dir}/terraform")
end

def terraform_installed_version
  require 'open3'
  stdout, stderr, status = Open3.capture3(
    "#{new_resource.install_dir}/terraform -v|head -1|awk '{print $2}'|sed -e 's/v//g'"
  )[0]
  return stdout.sub!("\n", "")
end

def cleanup
  ::File.delete("#{new_resource.install_dir}/terraform")
end

def install_terraform
  converge_by "Installing Terraform '#{new_resource.version}'" do
    begin
      unless Dir.exist?(new_resource.install_dir)
        require 'fileutils'
        FileUtils.mkdir_p "#{new_resource.install_dir}"
      end

      remote_file "/tmp/terraform_#{new_resource.version}_linux_amd64.zip" do
        source "https://releases.hashicorp.com/terraform/#{new_resource.version}/terraform_#{new_resource.version}_linux_amd64.zip"
        mode '0644'
        action :create
      end
      
      zipfile "/tmp/terraform_#{new_resource.version}_linux_amd64.zip" do
        into new_resource.install_dir
      end
      Chef::Log.debug("Installing Terraform version: #{new_resource.version}")
    rescue
      Chef::Log.error("Failed to install Terraform #{new_resource.version}")
    end
  end
end

action :uninstall do
  if terraform_is_installed
    converge_by "Uninstalling Terraform" do
      begin
        cleanup
        Chef::Log.debug("Uninstalling Terraform")
      rescue
        Chef::Log.error("Failed to uninstall Terraform")
      end
    end
  end
end
