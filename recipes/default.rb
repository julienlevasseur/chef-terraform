#
# Cookbook:: terraform
# Recipe:: default
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

terraform node['terraform']['version']

include_recipe 'terraform::plugins'
