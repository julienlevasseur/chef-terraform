#
# Cookbook:: chef-terraform
# Recipe:: plugins
#
# Copyright:: 2019, Julien Levasseur, All Rights Reserved.

node['terraform']['plugins'].each do |plugin|
  terraform_plugin plugin['name'] do
    version plugin['version']
    source plugin['source']
    destination plugin['destination']
  end
end
