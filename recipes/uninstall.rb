#
# Cookbook:: chef-terraform
# Recipe:: uninstall
#
# Copyright:: 2019, Julien Levasseur, All Rights Reserved.

terraform node['terraform']['version'] do
  action :uninstall
end
