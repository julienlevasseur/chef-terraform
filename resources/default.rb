actions :install, :uninstall
default_action :install

attribute :version, :kind_of => String, :name_attribute => true
attribute :install_dir, :kind_of => String, :default => "/usr/local/bin"