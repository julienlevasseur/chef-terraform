resource_name :terraform_plugin
provides :terraform_plugin

property :name, String, name_property: true
property :source, String, required: true
property :version, String
property :destination, String, default: '~/.terraform.d/plugins'

default_action :install

action :install do
  unless ::Chef::Recipe::Terraform::Plugin.installed?(new_resource.name, new_resource.destination)
    converge_by "Installing Terraform Plugin '#{new_resource.name}'" do
      begin
        Chef::Log.debug("Installing Terraform Plugin [#{new_resource.name}]")
        require 'fileutils'
        unless Dir.exist?("#{new_resource.destination}/#{::Chef::Recipe::Terraform::Architecture.get()}")
          FileUtils.mkdir_p "#{new_resource.destination}/#{::Chef::Recipe::Terraform::Architecture.get()}"
        end
        
        tar_extract new_resource.source do
          target_dir "#{new_resource.destination}/#{::Chef::Recipe::Terraform::Architecture.get()}"
        end

      rescue
        Chef::Log.error("Failed to install Terraform Plugin #{new_resource.name}")
      end
    end
  end
end
