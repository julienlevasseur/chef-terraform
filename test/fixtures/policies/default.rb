name 'default'
default_source :supermarket
default_source :chef_repo, '..'
cookbook 'terraform', path: '../../..'
run_list 'terraform::default'
