# # encoding: utf-8

# Inspec test for recipe terraform::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

require 'json'
nodefile = '/tmp/kitchen/nodes/default-ubuntu-1604.json'
node = json(nodefile).params if File.exist?(nodefile) # || json('/tmp/kitchen/nodes/default-ubuntu-1604.json').params

describe file('/usr/local/bin/terraform') do
  it { should exist }
end

describe command('/usr/local/bin/terraform -v|head -1') do
  its('stdout') { should match node['normal']['terraform']['version'] }
end
