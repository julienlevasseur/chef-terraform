# # encoding: utf-8

# Inspec test for recipe chef-terraform::plugins

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

require 'json'
nodefile = '/tmp/kitchen/nodes/node.json'
node = json(nodefile).params if File.exist?(nodefile) || json('/tmp/kitchen/nodes/node.json').params

node['normal']['terraform']['plugins'].each do |plugin|
  describe file("#{plugin['destination']}/linux_amd64/#{plugin['name']}") do
    it { should exist }
  end
end
